﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCBA_Hola_Mundo_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola mundo PCP 1-3";
            label1.BackColor = Color.Red;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adios mundo PCP 1-3";
            label1.BackColor = Color.Coral;
        }
    }
}
